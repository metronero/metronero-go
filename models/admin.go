package models

type AdminDashboardInfo struct {
	Stats          InstanceStats `json:"instance_stats"`
	RecentPayments []Payment     `json:"recent_payments"`
}
